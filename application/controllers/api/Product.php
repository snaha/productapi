<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
// use namespace
use Restserver\Libraries\REST_Controller;
use Predis\Cluster\PredisStrategy;
use Predis\Connection\Aggregate\PredisCluster;

class Product extends REST_Controller {

    function __construct()
    {
        $this->s3 = null;
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function uploadImages_post(){

        $image_path = $this->config->item('image_upload_path');
        //get the key
        $api_key = $this->rest->key;
        if(!empty($_FILES['userFiles']['name'])){
            $filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                $uploadPath = $image_path;
                $config['upload_path'] = $uploadPath;
                $config['encrypt_name'] = true;
                $config['allowed_types'] = 'gif|jpg|png';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_path'] = site_url($image_path.$fileData['file_name']);
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                }else
                    $this->set_response([
                        'status' => FALSE,
                        'message' => $this->upload->display_errors()
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }

            if(!empty($uploadData)){
                //resize asynchronously with suffix name
                //save to redis with key
                $client= $this->getPredisClient();
                if($client instanceof \Predis\Client){
                    $client->set($api_key, json_encode($uploadData));
                    $client->set($api_key.':time', time());
                }
                else
                    $this->set_response([
                        'status' => FALSE,
                        'message' => $client
                    ], REST_Controller::HTTP_NOT_FOUND, true);

                //resize using Gearman
                $this->load->library('Lib_gearman');
                if(config_item('use_gearman')
                    && config_item('gearman_for_resize')
                    && class_exists('Lib_gearman')
                    && $this->lib_gearman->is_supported()) {
                    $this->lib_gearman->gearman_client();
                    $this->lib_gearman->do_job_background('resizeImagesInBackground', serialize($uploadData));
                }
                //return
                $statusMsg = 'Files uploaded successfully';
                $this->set_response([
                        'status' => TRUE,
                        'message' => $statusMsg
                    ], REST_Controller::HTTP_OK); // FOUND (200) being the HTTP response code
            }
        }else
            $this->set_response([
                'status' => FALSE,
                'message' => 'Atleast one image is required'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    protected function getPredisClient(){
        $this->config->load('redis');
        require_once APPPATH . '/libraries/PredisDistributor.php';

        Predis\Autoloader::register();

        $multiple_servers = $this->config->item('multiple_servers');

        $options = array(
            'cluster' => function () {
                $distributor = new PredisDistributor();
                $strategy = new PredisStrategy($distributor);
                $cluster = new PredisCluster($strategy);
                return $cluster;
            }
        );

        try {
            $client = new Predis\Client($multiple_servers, $options);
            $client->connect();
        } catch(Predis\Connection\ConnectionException $e) {
            return $e->getMessage();
        }
        return $client;
    }

    public function add_post() {
        //load requirements
        $this->load->library('form_validation');
        $this->load->model('product_model');

        //validation part
        $this->form_validation->set_rules('title', 'Product Name', 'trim|required');
        $this->form_validation->set_rules('price', 'Price', 'trim|required');

        $form_validation_status = $this->form_validation->run();
        //error validation
        if($form_validation_status===FALSE)
            $this->set_response([
                'status' => FALSE,
                'message' => explode(PHP_EOL,trim(strip_tags(validation_errors())))
            ], REST_Controller::HTTP_NOT_FOUND, true);

        $title = $this->post('title');
        $price = $this->post('price');
        //adding products to database, images will be in redis as of now
        $new_product = $this->product_model->addProduct(array('title'=>$title,'price'=>$price));
        if($new_product['s']==false)
            $this->set_response([
                'status' => FALSE,
                'message' => "Oops! couldn't save product details"
            ], REST_Controller::HTTP_NOT_FOUND, true);

        $product_id = $new_product['id'];
        //getting API key for user
        $api_key = $this->rest->key;

        $Images = $this->getImagesFromPredis($api_key);

        $bucket = $this->s3Init('rest_api_products');

        //upload to s3
        // original images flow ,
        // this will keep all original images ready faster

        $this->uploadAllImagesToS3($Images, $product_id, $bucket);
        //send response then upload resized ones
        $this->set_response([
            'status' => TRUE,
            'data' => array('id'=>$product_id)
        ], REST_Controller::HTTP_OK);

        // 512 & 256 resize images flow

        $this->uploadAllResizedImagesToS3($Images, $product_id, $bucket);
    }

    public function get_get($id = null){
        $this->load->model('product_model');
        if(empty($id))
            $this->set_response([
                'status' => FALSE,
                'id' => 'ID of product is required'
            ], REST_Controller::HTTP_NOT_FOUND, true);

        $product_info = $this->product_model->getProductInfo($id);
        if(!empty($product_info)){
            unset($product_info['id']);
            $images = $this->product_model->getProductImages($id);
            if(empty($images)){//get from Redis
                $client =$this->getPredisClient();
                if($client instanceof Predis\Client)
                    $images = $client->get($this->rest->key);
                else $images = array();
            }
            $images_512 = array();
            $images_256 = array();
            $resize_suffix = $this->config->item('img_resize_suffix');
            $ori_images= array();
            foreach ($images as $img) {
                $img_link = empty($img['image_url'])? $img['file_path']:$img['image_url'];
                $height = $img['height'];
                $width = $img['width'];
                $ori_images[] = array('img'=>$img_link,'height'=>$height,'width'=>$width);
                $exp =explode('.', $img_link);
                $ext = '.'.$exp[sizeof($exp)-1];
                $first = explode($ext, $img_link)[0];

                $height = ($width==0)? 0 : intval($height/$width*512);
                $images_512[] = array('img'=>$first.$resize_suffix[512].$ext,'height'=>$height,'width'=>512);
                $height = ($width==0)? 0 : intval($height/$width*256);
                $images_256[] = array('img'=>$first.$resize_suffix[256].$ext,'height'=>$height,'width'=>256);
            }

            $product_info['images'] = $ori_images;
            $product_info['images_256'] = $images_256;
            $product_info['images_512'] = $images_512;
        }else
            $product_info = array();
        $this->set_response([
            'status' => TRUE,
            'data' => array('product'=>$product_info)
        ], REST_Controller::HTTP_OK, true);
    }

    private function uploadAllImagesToS3($Images, $product_id, $bucket){
        foreach($Images as $image){
            $image_path = $image->file_path;
            $image_size = getimagesize($image_path);
            $filename = $product_id.'/'.filePath($image_path)['basename'];

            $upload_stat = $this->uploadImagesToS3($bucket , $image_path, $filename);
            $s3url = $this->config->item('s3domain').$bucket.'/'.$filename;

            if($upload_stat)
                $this->product_model->addImagesToProduct($product_id, $s3url, $image_size[0], $image_size[1]);
        }
    }

    private function uploadAllResizedImagesToS3($Images, $product_id, $bucket){

        foreach($Images as $image){
            $image_path = $image->file_path;
            //512
            $resized_file_1 = getResizeImagePath($image_path, 512);
            $status1 = reSizeImageUsingImagick($image_path, $resized_file_1, 512, 0);
            if(!$status1)
                $resized_file_1 = null;

            //256
            $resized_file_2 = getResizeImagePath($image_path, 256);
            $status2 = reSizeImageUsingImagick($image_path, $resized_file_2, 256, 0);
            if(!$status2)
                $resized_file_2 = null;

            //upload to s3
            if($resized_file_1){
                $filename_512 = $product_id.'/'.filePath($resized_file_1)['basename'];
                $this->uploadImagesToS3($bucket , $resized_file_1, $filename_512);
            }

            if($resized_file_2){
                $filename_256 = $product_id.'/'.filePath($resized_file_2)['basename'];
                $this->uploadImagesToS3($bucket , $resized_file_2, $filename_256);
            }
        }
    }

    private function uploadImagesToS3($bucket, $img_url, $filename){
        if(empty($img_url) || empty($bucket) || !file_exists($img_url))
            return false;
        $upload_status = $this->s3->putObjectFile($img_url, $bucket, $filename, S3::ACL_PUBLIC_READ);

        return $upload_status;
    }

    private function getImagesFromPredis($key){
        $client = $this->getPredisClient();
        if(!$client instanceof Predis\Client)
            return array();
        $images = $client->get($key);
        return json_decode($images);
    }


    private function s3Init($bucket_name) {
        if(!$this->s3){
            $awsAccessKey = config_item('S3AccessKey');
            $awsSecretKey = config_item('S3SecretKey');

            require_once(config_item('ext_lib_dir')."/S3.php");

            $s3 = new S3($awsAccessKey, $awsSecretKey);
            $this->s3 = $s3;
            try{
                $buckets = $s3->listBuckets();
                foreach($buckets as $bucket){
                    if($bucket==$bucket_name)
                        return $bucket_name;
                }
                $s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ);
            }catch (Exception $e){}
        }
        return $bucket_name;
    }
}
