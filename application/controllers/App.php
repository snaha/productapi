<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Predis\Cluster\PredisStrategy;
use Predis\Connection\Aggregate\PredisCluster;

class App extends CI_Controller {
    public function clearCacheImageInfo()
    {
        $this->load->model('user_model');

        $all_keys = $this->user_model->getAllKeys();

        $client = $this->getPredisClient();
        foreach ($all_keys as &$key){
            $create_time =$client->get($key.':time');
            if(time()-$create_time > 3600) {//1 hour before
                $images = $client->get($key);
                foreach ($images as $image){
                    unlink($image['file_path']);
                    unlink(getResizeImagePath($image['file_path'], 256));
                    unlink(getResizeImagePath($image['file_path'], 512));
                }
                $client->del([$key,$key.':time']);
            }
        }
    }

    protected function getPredisClient(){
        $this->config->load('redis');
        require_once APPPATH . '/libraries/PredisDistributor.php';

        Predis\Autoloader::register();

        $multiple_servers = $this->config->item('multiple_servers');

        $options = array(
            'cluster' => function () {
                $distributor = new PredisDistributor();
                $strategy = new PredisStrategy($distributor);
                $cluster = new PredisCluster($strategy);
                return $cluster;
            }
        );

        try {
            $client = new Predis\Client($multiple_servers, $options);
            $client->connect();
        } catch(Predis\Connection\ConnectionException $e) {
            return $e->getMessage();
        }
        return $client;
    }
}
