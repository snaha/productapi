<?php

function error_file_log($method, $message, $file_name = 'seenit-error.log') {
    $ci =& get_instance();
    $log_message = date('Y-m-d H:i:s') . " : " . $method . " : " . $message . "\n\n";
    $is_live = $ci->config->item('is_live');
    if (!$is_live)
        error_log($log_message);
    else error_log($log_message, 3, '/var/log/apache2/'.$file_name);
}

function filePath($filePath)
{
    $fileParts = pathinfo($filePath);

    if(!isset($fileParts['filename']))
    {$fileParts['filename'] = substr($fileParts['basename'], 0, strrpos($fileParts['basename'], '.'));}

    return $fileParts;
}

function getResizeImagePath($filePath, $img_size)
{
    $static = !(isset($this) && get_class($this) == __CLASS__);
    if($static)
        $ci = &get_instance();
    else
        $ci = $this;
    if(empty($img_size))
        return $filePath;
    if(empty($filePath))
        return false;
    $fileParts = filePath($filePath);

    $file_name_exp = explode('.', $fileParts['basename']);

    $resize_suffix = $ci->config->item('img_resize_suffix');

    $fullpath = $fileParts['dirname'].'/'.$file_name_exp[0].$resize_suffix[$img_size.''].'.'.$file_name_exp[1];
    return $fullpath;
}

function reSizeImageUsingImagick($source, $target, $width, $height =0){
    $thumb = new Imagick();
    if(!$source || !$width)
        return false;
    if(!file_exists($source))
        return false;
    $thumb->readImage($source);
    $thumb->resizeImage($width, $height, Imagick::FILTER_LANCZOS,1);
    $thumb->writeImage($target);
    $thumb->clear();
    $thumb->destroy();
    return true;
}