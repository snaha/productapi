<?php

class Product_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function addProduct($data) {
        if(empty($data) || empty($data['title']) || empty($data['price']))
            return array('s'=>false);
        $result = $this->db->query('insert into `products` (`title`,`price`) VALUES (?, ?) ', array($data['title'], $data['price']));
        if($result) {
            return array('s'=>true,'id' =>$this->db->insert_id());
        } else {
            error_log("FAILQUERY : product model : addProduct : data =".json_encode($data));
            return array('s'=>false);
        }
    }

    function addImagesToProduct($product_id, $img, $width=0, $height=0) {
        if(empty($product_id) || empty($img))
            return array('s'=>false);
        $result = $this->db->query('insert into product_images (`product_id`,`image_url`,`width`,`height`) value (?, ?) ', array($product_id, $img, intval($width), intval($height)));
        if($result) {
            return array('s'=>true);
        } else {
            error_log("FAILQUERY : product model : addImagesToProduct : id = $product_id");
            return array('s'=>false);
        }
    }

    function getProductInfo($product_id) {
        if(empty($product_id))
            return array();
        $result = $this->db->query('select * from products where id = ? ', array($product_id));
        if($result) {
            return $result->row_array();
        } else {
            error_log("FAILQUERY : product model : getProductInfo : id = $product_id");
            return array();
        }
    }

    function getProductImages($product_id) {
        if(empty($product_id))
            return array();
        $result = $this->db->query('select image_url,height,width from product_images where product_id = ? ', array($product_id));
        if($result) {
            return $result->result_array();
        } else {
            error_log("FAILQUERY : product model : getProductImages : id = $product_id");
            return array();
        }
    }


}