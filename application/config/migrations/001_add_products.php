<?php
class Migration_Add_Products extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 60,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '400',
                ),
                'price' => array(
                    'type' => 'TEXT',
                    'null' => true
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('products');
    }

    public function down()
    {
        $this->dbforge->drop_table('products');
    }
}