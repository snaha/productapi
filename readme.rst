###################
Simple Product Rest API
###################

- Design a service to upload images to a server
	- The images should be uploaded in two or more requests one for transferring images
- Another for uploading the details of the product
	- Not advisable to upload the images in the same request since the greater the payload
	more the chances of the request being dropped in slow internet connections

###################
Features
###################
-Predis to cache image info

-Gearman to asynchronously resize images

-Redis cluster to handle multiple server cache availability

-Restful

-Usage of Amazon S3 for images

*******************
How it should work
*******************

- The APIs should roughly work this way
	  - An upload image api which saves the data in a key value store like redis
	  - The following api for uploading product details which sends the rest of the product data
      -This api should save the data in some location(AWS S3) determined by the ID of the product in the DB
      - Use your favourite SQL DB to store the product info
	  - The uploaded images should be then resized asynchronously in 2 different file sizes 256 and 512 pixel width
	  - Resized image's data should be stored in DB
	  - Setup a cron job to delete the transferred images from REDIS
	  - Provide a product fetch API for querying stored products, this should send all the prouducts data along with the
	  images. With the images you need to send the dimensions of the image too
- If you have more than one server you need access to some fast store
    shared across the servers so that the second request which carries the product data is able to use the images
- The other option is to ensure that your load balancer redirects both the requests to the same server. This
	is a bit tricky to get right
	- Products will have the following details
	  - ID
	  - Name
	  - image
	  - image_256 ( Resized images )
	  - image_512 ( Resized images )
	  - price
**************************
Installation
**************************

- Add AWS S3 credentials to config/config.php in S3AccessKey and S3SecretKey keys
-Install Gearman : sudo apt-get install gearman-job-server [use ImageResizeWorker.sh & ImageResizeWorker_script.conf]
-Install Gearman-php extension :  sudo pecl install channel://pecl.php.net/gearman-1.1.2
-Add extension=gearman.so in php.ini
-You can use Supervisor to keep Gearman running : https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps
-Install Redis and configure redis cluster [ports used : 7000 - 7002]
-Run the Gearman working using : sudo php root/index.php gearman handleImageResizeJob
-root/uploads/files should be writable

**************************
API flow
**************************

-PUT /index.php/api/key to generate key

-POST /index.php/api/product/uploadImages to upload multiple images

-POST /index.php/api/product/add to submit product details

-GET /index.php/api/product/get/1 to get details of product with id 1

*******************
Server Requirements
*******************

PHP version 5.2 - 6.0 is recommended.

*********
Resources
*********

-  `User Guide https://github.com/appleboy/CodeIgniter-Gearman-Library`
-  `Redis cluster https://redis.io/topics/cluster-tutorial`
-  `https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps`
