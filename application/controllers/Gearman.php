<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gearman extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public static function resizeImages($data){
        if(!is_cli()) {
            die("Not allowed to run from the browser");
        }
        $data = unserialize($data->workload());
        $CI = & get_instance();
        $CI->load->helper('base');
        foreach($data as $row){
            $image_path = $row['file_path'];
            //512 width
            $resized_file_1 = getResizeImagePath($image_path, 512);
            $status1 = reSizeImageUsingImagick($image_path, $resized_file_1, 512, 0);
            //256 width
            $resized_file_2 = getResizeImagePath($image_path, 256);
            $status2 = reSizeImageUsingImagick($image_path, $resized_file_2, 256, 0);

            if(!$status1 || !$status2)
                error_log("Image resize failed : ". (!$status1)?$resized_file_1 : "" ." " . (!$status2)?$resized_file_2 : "");
        }
    }


    public function handleImageResizeJob() {
        if(!is_cli()) {
            die("Not allowed to run from the browser");
        }
        $this->load->library('Lib_gearman');
        $worker = $this->lib_gearman->gearman_worker();
        $this->lib_gearman->add_worker_function('resizeImagesInBackground', 'Gearman::resizeImages');

        while ($this->lib_gearman->work()) {
            if (!$worker->returnCode()) {
                //echo " processing completed : time : ".date('Y-m-d H:i:s')."\n";
            }
            if ($worker->returnCode() != GEARMAN_SUCCESS) {
                echo "return_code: " . $this->lib_gearman->current('worker')->returnCode() . "\n";
                error_file_log(__FUNCTION__,"return_code: " . $this->lib_gearman->current('worker')->returnCode() ." ".date('Y-m-d H:i:s'). "\n",'gearman_log.log');
                break;
            }
        }
        error_log("Gearman :".__FUNCTION__." :exiting...");
    }
}