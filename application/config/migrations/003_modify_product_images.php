<?php
class Migration_Modify_Product_Images extends CI_Migration
{
    public function up()
    {
        $this->db->query("alter table product_images add column `height` INT(10) default 0");
        $this->db->query("alter table product_images add column `width` INT(10) default 0");
    }

    public function down()
    {
        $this->db->query("alter table product_images drop column height ");
        $this->db->query("alter table product_images drop column height ");
    }
}