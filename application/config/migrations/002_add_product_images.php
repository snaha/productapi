<?php
class Migration_Add_Product_Images extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 60,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'product_id' => array(
                    'type' => 'INT',
                    'constraint' => 60,
                ),
                'image_url' => array(
                    'type' => 'TEXT',
                    'constraint' => 400,
                    'null' => false
                )
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('product_images');
    }

    public function down()
    {
        $this->dbforge->drop_table('product_images');
    }
}