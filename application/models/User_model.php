<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAllKeys() {
        $result = $this->db->query('select `key` from `keys` ');
        if($result) {
            return $result->result_array();
        } else {
            error_log("FAILQUERY : user model : getAllKeys");
            return array();
        }
    }

}