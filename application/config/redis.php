<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Memcached settings
| -------------------------------------------------------------------------
| Your Memcached servers can be specified below.
|
|	See: https://codeigniter.com/user_guide/libraries/caching.html#memcached
|
*/
$config['single_server'] = array(
    'host' => '127.0.0.1',
    'port' => 6379
);
$config['multiple_servers'] = array(
    array(
        'host' => '127.0.0.1',
        'port' => 7000,
        'alias' => 'first'
    ),
    array(
        'host' => '127.0.0.1',
        'port' => 7001,
        'alias' => 'second'
    ),array(
        'host' => '127.0.0.1',
        'port' => 7002,
        'alias' => 'third'
    )
);